/**
 *	Component to fade background images.
 *	@author		Christian Würker <christian.wuerker@seefeuer.net>
 *	@copyright	2017 Seefeuer GbR <https://seefeuer.net>
 */
var BackgroundImageFadeShow = {
	container: null,															//  container to change images for
	current: 0,																	//  currently set image
	images: [],																	//  nr of images available
	durationShow: 10000,														//  time between transitions
	durationFade: 2000,															//  transition time
	init: function(images, durationShow, durationFade){
		var container, i, layer, time;
		this.images = images;
		this.durationShow = durationShow;										//  time between transitions
		this.durationFade = durationFade;										//  transition time
		this.container = jQuery("<div></div>").prependTo(jQuery("body"));
		this.container.attr("id", "background-layers");
		for(i=0; i<images.length; i++){
			layer = jQuery("<div></div>").addClass("background-layer");
			layer.css("background-image", "url("+this.images[i])+")";
			this.container.append(layer);
		}
		this.container.find("div").eq(this.current).show();
		time = this.durationShow + this.durationFade;
		this.interval = window.setInterval(this.nextImage, time);				//  set interval for image change
	},
	nextImage: function(){
		var _this = BackgroundImageFadeShow;									//  shortcut component
		var curr = _this.current;												//  increase current image counter
		var next =  curr + 1 < _this.images.length ? curr + 1 : 0;
		_this.container.find("div").eq(curr).fadeOut(_this.durationFade);
		_this.container.find("div").eq(next).fadeIn(_this.durationFade);
		_this.current = next;													//  note new current image
	}
};
